<?php

require '../Db.php';

/**
* Base Active record model
*/
class ActiveRecord
{
	
	protected $db;
	protected $dbTable;
	protected $primary;

	//i want a place to store the fields for an object, it will look at this object 
	//to type the properties of on object. this will hopefully prevent the neccesaty to
	//get the fields and types and set all to null and and and.
	protected $container;

	function __construct($db = null)
	{
		if ($db) {
			$this->setDb($db);
		} else {
			$db = Db::getInstance();
			$this->setDb($db);
		}

	}

	public function setDb($db)
	{
		$this->db = $db;
	}

	public function getDb()
	{
		return $this->db;
	}

	public function setDbTable($dbTable)
	{
		$this->dbTable = $dbTable;
	}

	public function getDbTable()
	{
		return $this->dbTable;
	}

	function setPrimary($primary)
	{
		$this->primary = $primary;
		return $this;
	}
	
	function getPrimary()
	{
		if (null === $this->primary) {
			// code
		}
		return $this->primary;
	}
	

	public function save()
	{
		$primary = $this->primary;
		$id = $this->$primary;
		if (null === $id) {
			$this->db->insert($this->dbTable, $this);
		} else {
			$this->db->update($this->dbTable, $this, $primary . ' = ' . $id);
		}
	}

	public function toArray()
	{
		$properties = get_object_vars($this);
		$properties = array_slice($properties, 0, -4);
		return $properties;
	}

	public function getFields()
	{
		$properties = get_class_vars(get_class($this));
		$properties = array_slice($properties, 0, -4);
		return $properties;
	}

	public function setAllToNull()
	{
		foreach ($this as $key => $value) {
			$this->$key = null;
		}
	}
}

