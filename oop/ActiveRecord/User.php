<?php

/**
* User Model to test ActiveRecord
*/
class User extends ActiveRecord
{
	public $user_id = array('type' => 'int', 'primary' => true, 'autonumber' => true);
	public $first_name = array('type' => 'string');
	public $last_name = array('type' => 'string');
	public $email = array('type' => 'string');
	public $groups = array('type' => 'relationship', 'model' => 'groups');

}