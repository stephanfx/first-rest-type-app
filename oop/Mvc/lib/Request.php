<?php

/**
* Request class
*/
class Request
{
	public $url_elements;
	public $parameters;

	function __construct()
	{
		$this->url_elements = explode('/', $_SERVER['PATH_INFO']);
		$this->parseParameters();
	}

	public function parseParameters()
	{
		parse_str($_SERVER['QUERY_STRING'], $this->parameters);
	}
}