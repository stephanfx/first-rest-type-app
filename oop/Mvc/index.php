<?php

function mvcAutoload($className)
{
	if (preg_match('/[a-zA-Z]+Controller$/', $className)) {
		include __DIR__ . '/controllers/' . $className . '.php';
		return true;
	}
	if (preg_match('/[a-zA-Z]+Model$/', $className)) {
		include __DIR__ . '/models/' . $className . '.php';
		return true;
	}
	if (preg_match('/[a-zA-Z]+View$/', $className)) {
		include __DIR__ . '/views/' . $className . '.php';
		return true;
	} else {
		include __DIR__ . '/lib/' . str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';
		return true;
	}
	return false;
}

spl_autoload_register('mvcAutoload');

$request = new Request();

$controllerName = ucfirst($request->url_elements[1]) . 'Controller';
if (class_exists($controllerName)) {
	$controller = new $controllerName();
	$action = $request->url_elements[2] . "Action";
	$result = $controller->$action($request->parameters);
	$viewName = $request->url_elements[2] . "View";
	if (class_exists($viewName)) {
		$view = new $viewName();
		$view->render($result);
	}
	
}