<?php

/**
* Example of the factory pattern, another would be the requests of a webservice?
*/
class Configuration
{
	const STORE_INI = 1;
	const STORE_XML = 2;
	const STORE_JSON = 3;

	public static function getStore($type = self::STORE_INI)
	{
		switch ($type) {
			case 1:
				return new Configuration_Ini();
				break;
			
			case 2:
				return new Configuration_Xml();
				break;
			
			case 3:
				return new Configuration_Json();
				break;
			
			default:
				# code...
				break;
		}
	}
}

/**
* simple class to satisfy above
*/
class Configuration_Ini
{
	
	function __construct()
	{
		
	}
}

/**
* simple class to satisfy factory request
*/
class Configuration_Xml
{
	
	function __construct()
	{
	
	}
}

/**
* simple class to satisfy factory request
*/
class Configuration_Json
{
	
	function __construct()
	{
	}
}

var_dump(Configuration::getStore(Configuration::STORE_XML));