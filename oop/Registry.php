<?php

/**
* Registry pattern 
*/
class Registry
{
	private static $register;

	public static function add(&$item, $name = null)
	{
		if (is_object($item) && is_null($name)) {
			$name = get_class($item);	
		} elseif (is_null($name)) {
			$msg = "You must provide a name for none objects";
			throw new Exception($msg, 1);
		}

		$name = strtolower($name);
		self::$register[$name] = $item;
	}

	public static function &get($name)
	{
		$name = strtolower($name);
		if (array_key_exists($name, self::$register)) {
			return self::$register[$name];	
		} else {
			$msg = "$name is not registered.";
			throw new Exception($msg, 1);
		}
	}

	public static function exists($name)
	{
		$name = strtolower($name);
		if (array_key_exists($name, self::$register)) {
			return true;
		} else {
			return false;
		}
	}
}

include 'Db.php';

$db = Db::getInstance();
Registry::add($db);
var_dump(Registry::exists('Db'));
var_dump(Registry::get('Db'));