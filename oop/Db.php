<?php

/**
* This is a db class implementing the Singleton pattern
*/
class Db
{
	private static $singleton;
	private $connection = null;

	private function __construct()
	{
		
	}

	public static function getInstance()
	{
		if (null === self::$singleton) {
			self::$singleton = new Db();
		}
		return self::$singleton;
	}

	public function getConnection(array $config = null)
	{
		if (null == $this->connection OR null !== $config) {
			$host = $config['host'];
			$dbname = $config['dbname'];
			$username = $config['username'];
			$password = $config['password'];
			$dsn = "mysql:host=$host;dbname=$dbname";
			$this->connection = new PDO($dsn, $username, $password);
		}
		return $this->connection;
	}
}
