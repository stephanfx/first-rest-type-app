<?php
/**
 * 	User Class
 */
class UserController extends MyController
{
    
    public function getAction($request)
    {
        $model   = new UserModel();
        $users   = $model->getUsers();
        if (isset($request->url_elements[2])) {
            $user_id = (int)$request->url_elements[2];
            if (isset($request->url_elements[3])) {
                
                switch ($request->url_elements[3]) {
                    case 'friends':
                        $data['message'] = "User " . $users[$user_id]['name'] . " has a lot of friends";
                        
                        break;

                    default:
                        $data['message'] = "Not a supported action";
                        
                        break;
                }
            }
            else {
                $data['message'] = "Here is the info for the user: " . $users[$user_id]['name'];
            }
        }
        else {
            $data['message'] = "you want a list of users";
            $data['users'] = $users;
        }
        
        return $data;
    }
}
