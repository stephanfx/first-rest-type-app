<?php

/**
* Json View
*/
class JsonView extends ApiView
{
	
	function render($content)
	{
		header('Content-Type: application/json; charset=utf8');
		echo json_encode($content);
		return true;
	}
}