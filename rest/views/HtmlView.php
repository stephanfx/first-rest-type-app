<?php

/**
* Json View
*/
class HtmlView extends ApiView
{
	
	function render($content)
	{
		header('Content-Type: text/html; charset=utf8');
		echo '<pre>';
		var_dump($content);
		echo '</pre>';
		return true;
	}
}