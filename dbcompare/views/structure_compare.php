<?php 
	if (count($results) > 0) {
		
		echo '<h3>Found differences in ' . count($results) . ' table' . s(count($results)) . ':</h3>';
		
		echo '<ul id="differences">';
		foreach ($results as $table_name => $differences) {
			
			echo '<li><strong>' . $table_name . '</strong><ul>';
			foreach ($differences as $difference) {
				echo '<li>' . $difference . '</li>';
			}
			echo '</ul></li>';
		}
		echo '</ul>';
		
	} else {
		echo '<p>No differences found.</p>';
	}
?>
	<h3>Match Table Data</h3>

	<form action="?a=compareData" method="post">
		<?php echo $tables; ?>
		<input type="hidden" value="<?php echo $_POST['db1']; ?>" name="db1">
		<input type="hidden" value="<?php echo $_POST['db2']; ?>" name="db2">
		<input type="submit" value="Compare Data">
	</form>
