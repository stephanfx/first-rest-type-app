<h3>Step 1: Export database schemas</h3>
<?php 	if (count($dbs_config) > 0) : ?>
		
		<p class="info">Select a database configuration from the list below, or select 'Enter details...'</p>
		
		<form action="?a=compare" method="post" id="compare">
		Schema 1: <select name="db1">
		<?php foreach ($dbs_config as $key => $db_config) : ?>
			<option value="<?php echo $key; ?>"><?php echo $db_config['name'] ?></option>
		<?php endforeach; ?>
		</select>

		Schema 2: <select name="db2">
		<?php foreach ($dbs_config as $key => $db_config) : ?>
			<option value="<?php echo $key; ?>"><?php echo $db_config['name'] ?></option>
		<?php endforeach; ?>
		</select>
		<input type="submit" id="Compare" name="Compare" value="Compare">
		</form>
			
<?php else : ?>		
		
		<p class="info">Enter connection details in the form below, or setup a database connection in the <code>config.php</code> file.</p>'
	
<?php endif; ?>	

	<form action="?a=export_schema" method="post" id="db-config" <?php echo  (count($dbs_config) > 0) ? ' style="display:none;"' : ''; ?>>
	<div class="field"><label for="db-host">Host</label><input type="text" name="db-host" id="db-host" value="localhost" /></div>
	<div class="field"><label for="db-user">User</label><input type="text" name="db-user" id="db-user" /></div>
	<div class="field"><label for="db-password">Password</label><input type="password" name="db-password" id="db-password" /></div>
	<div class="field"><label for="db-name">Database</label><input type="text" name="db-name" id="db-name" /></div>
	<div class="submit"><input type="submit" value="Export" /></div><div class="clearer"></div>
	</form>
	
	<h3>Step 2: Compare schemas</h3>
	
	<p class="info">Once two database schemas have been exported, paste them here to be compared.</p>
	
	<form action="?a=compare" method="post" id="compare">
	<div class="field"><label for="schema1">First schema</label><textarea name="schema1" id="schema1" cols="100" rows="5"></textarea></div>
	<div class="field"><label for="schema2">Second schema</label><textarea name="schema2" id="schema2" cols="100" rows="5"></textarea></div>
	<div class="submit"><input type="submit" value="Compare" /></div>
	</form>
