<?php
error_reporting(E_ALL);

require_once('/model/DbDiff.php');
require_once('/model/Methods.php');
require('config.php');
?>
<!DOCTYPE html>

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>DbDiff</title>
	
	<link rel="stylesheet" href="style.css" type="text/css" media="screen" />
	
</head>
<body>
	
<div id="canvas">
	
<h1><a href="?">DbDiff</a></h1>
<h2>Tool for comparing database schemas.</h2>

<?php

$action = @$_GET['a'];


switch ($action) {
	
	case 'export_schema':
	
		if (isset($_GET['db'])) {
			
			$db = $_GET['db'];
	
			if (!isset($dbs_config[$db])) {
				echo_error('No database configuration selected.');
				break;
			}

			$config = $dbs_config[$db];
			
		} else {
			
			
				
			$config = array(
				'name' => $_POST['db-name'] . ' (' . $_POST['db-host'] . ')',
				'config' => array(
					'host' => $_POST['db-host'],
					'user' => $_POST['db-user'],
					'password' => $_POST['db-password'],
					'name' => $_POST['db-name']
				)
			);
		}
		
		
		
		echo '<p><a href="?">&laquo; Back to main page</a></p>';
		
		break;
		
	case 'compare':

		if ($_POST['db1'] == $_POST['db2']) {
			echo_error('Database cant be compared to itself.');
			break;
		}
		
		$config1 = $dbs_config[$_POST['db1']];
		$config2 = $dbs_config[$_POST['db2']];

		$schema1 = export_schema($config1);
		$schema2 = export_schema($config2);

		$results = do_compare($schema1, $schema2);
		$tables = DbDiff::getTables($config1['config']);
		include_once 'views/structure_compare.php';
		
		echo '<p><a href="?">&laquo; Back to main page</a></p>';
		
		break;
	case 'compareData' :
		foreach ($_POST	as $key => $value) {
			if (stristr($key, 'table')) {
				echo '<h3>Running on ' . $value . '</h3>';
				$dataSet1 = DbDiff::exportData($dbs_config[$_POST['db1']]['config'], $value);
				$dataSet2 = DbDiff::exportData($dbs_config[$_POST['db2']]['config'], $value);

				$diffs = DbDiff::compareData($dataSet1, $dataSet2);
				
			}
		}

		include_once 'views/compare-data.php';	
		echo '<p><a href="?">&laquo; Back to main page</a></p>';
		
		break;

	default:
	
		include_once 'views/show-options.php';
}

?>

<div id="footer">
	
</div>

</div>

</body>
</html>