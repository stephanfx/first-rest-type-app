<?php

/**
 * This provides a user-interface for using the DbDiff class.
 * 	
 * More information on this tool can be found at:
 * http://joefreeman.co.uk/blog/2009/07/php-script-to-compare-mysql-database-schemas/
 * 
 * Copyright (C) 2009, Joe Freeman <joe.freeman@bitroot.com>
 * Available under http://en.wikipedia.org/wiki/MIT_License
 */






/**
 * Convenience method for outputting errors.
 *
 * @return void
 **/
function echo_error($error) {
	echo '<p class="error">', $error, '</p>';
}

/**
 * Export the schema from the database specified and echo the results.
 *
 * @param string $db The key of the config to be extracted from $dbs_config.
 * @return void
 */
function export_schema($config) {
	
	$result = DbDiff::export($config['config'], $config['name']);
	
	if ($result == null) {
		echo_error('Couldn\'t connect to database: ' . mysql_error());
		return;
	}
	
	$serialized_schema = serialize($result);
	
	return $serialized_schema;
}

/**
 * Strips new line characters (CR and LF) from a string.
 *
 * @param string $str The string to process.
 * @return string The string without CRs or LFs.
 */
function strip_nl($str) {
	return str_replace(array("\n", "\r"), '', $str);
}

/**
 * Returns an 's' character if the count is not 1.
 * 
 * This is useful for adding plurals.
 *
 * @return string An 's' character or an empty string
 **/
function s($count) {
	return $count != 1 ? 's' : '';
}

/**
 * Compare the two schemas and echo the results.
 *
 * @param string $schema1 The first schema (serialized).
 * @param string $schema2 The second schema (serialized).
 * @return void
 */
function do_compare($schema1, $schema2) {
	
	if (empty($schema1) || empty($schema2)) {
		echo_error('Both schemas must be given.');
		return;
	}
	
	$unserialized_schema1 = unserialize(strip_nl($schema1));
	$unserialized_schema2 = unserialize(strip_nl($schema2));
	
	$results = DbDiff::compare($unserialized_schema1, $unserialized_schema2);
	return $results;	
	
}

?>