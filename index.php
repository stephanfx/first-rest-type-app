<?php 

$greet = function($name) {
	printf("Hello %s\r\n", $name);
};

$greet("PHP");
$greet("World");

$obj = new StdClass();

$obj->sayHello = function($name) {
	printf("Hi %s\n", $name);
};

$greet2 = $obj->sayHello;
$greet2("Stephan");
// $obj->sayHello("Stephan");

$arr['func'] = function(){
	printf("Hello %d %s", 15, "Worlds");
};

$arr['func']();

$a = array(1,3,5,7,9);
$b = array_map(function($n){ 
	return ($n * $n);
}, $a);
print_r ($b);